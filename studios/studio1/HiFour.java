package studio1;

import cse131.ArgsProcessor;

/**
 * From Sedgewick and Wayne, COS 126 course at Princeton
 * 
 */
public class HiFour {
	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		String s0 = ap.nextString("Enter First Person's Name");
		String s1 = ap.nextString("Enter Second Person's Name");
		String s2 = ap.nextString("Enter Third Person's Name");
		String s3 = ap.nextString("Enter Fourth Person's Name");
		//
		// Say hello to the names in s0 through s3.
		//
		System.out.println("Hello " + s0 + ", " + s1 + ", " + s2 + ", and " + s3 + "!" );
	}
}
